// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5;
	SpeedBoost = 1;
	LastMoveDirection = EMovementDirection::LEFT;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MovementSpeed*SpeedBoost);
	Move();

}

void ASnakeBase::SpeedUp(int a)
{
	SetActorTickInterval(MovementSpeed * a);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0;i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		//int32 ElemIndex = SnakeElements.Pop(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			//NewSnakeElem->MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
		}


	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	//float MovementSpeed = ElementSize;//���� ��� ������ ����� 
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;//MovementSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;//MovementSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;//MovementSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;//MovementSpeed;
		break;
	}

	SnakeElements[0]->ToggleCollision();
	//AddActorWorldOffset(MovementVector);
	for (int i = SnakeElements.Num() - 1;i > 0;i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	int32 ElemIndex;
	SnakeElements.Find(OverlappedElement, ElemIndex);
	bool bIsFirst = ElemIndex == 0;

	IInteractable* InteractableInterface = Cast<IInteractable>(Other);
	if (InteractableInterface)
	{
		InteractableInterface->Interact(this, bIsFirst);
	}
}

